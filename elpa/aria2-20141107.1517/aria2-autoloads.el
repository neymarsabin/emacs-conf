;;; aria2-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (or (file-name-directory #$) (car load-path)))

;;;### (autoloads nil "aria2" "aria2.el" (22294 21465 449417 799000))
;;; Generated autoloads from aria2.el

(autoload 'aria2-downloads-list "aria2" "\
Display aria2 downloads list.  Enable `aria2-mode' to controll the process.

\(fn)" t nil)

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; aria2-autoloads.el ends here
