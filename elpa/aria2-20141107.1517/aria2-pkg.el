(define-package "aria2" "20141107.1517" "Control aria2c commandline tool from Emacs" '((emacs "24.4")) :url "https://bitbucket.org/ukaszg/aria2-mode" :keywords '("download" "bittorrent" "aria2"))
