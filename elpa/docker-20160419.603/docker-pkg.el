(define-package "docker" "20160419.603" "Emacs interface to Docker"
  '((emacs "24.4")
    (dash "2.12.1")
    (magit-popup "2.6.0")
    (s "1.11.0")
    (tle "0.2.1"))
  :url "https://github.com/Silex/docker.el")
;; Local Variables:
;; no-byte-compile: t
;; End:
