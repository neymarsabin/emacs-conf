(define-package "docker-api" "20160420.2353" "Emacs interface to the Docker API"
  '((dash "2.12.1")
    (s "1.11.0"))
  :url "https://github.com/Silex/docker-api.el")
;; Local Variables:
;; no-byte-compile: t
;; End:
