(define-package "ob-prolog" "20150530.937" "org-babel functions for prolog evaluation." 'nil :url "https://github.com/ljos/ob-prolog" :keywords '("literate programming" "reproducible research"))
