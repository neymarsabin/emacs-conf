;ELC   
;;; Compiled
;;; in Emacs version 24.5.1
;;; with all optimizations.

;;; This file uses dynamic docstrings, first added in Emacs 19.29.

;;; This file does not contain utf-8 non-ASCII characters,
;;; and so can be loaded in Emacs versions earlier than 23.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(byte-code "\301\302!\210\301\303\304\"\203 \305\202 \306!\210\301\307!\210\301\310!\210\301\311!\210\301\312!\210\313\314\315\316\317\320\321\322\323\302&	\210\324\325\315\326\327\330\331\314\323\314&	\210\332\333\325\"\210\324\334\315\335\327\330\331\314\323\314&	\210\332\336\334\"\210\324\337\315\340\327\330\331\314\323\314&	\210\332\341\337\"\210\324\342\315\343\327\330\331\314\323\314&	\210\332\344\342\"\210\324\345\315\346\327\330\331\314\323\314&	\210\332\347\345\"\207" [emacs-version require org version< "24.4" org-publish ox-publish dash-functional s deferred ido custom-declare-group org2jekyll nil "Publish org-mode posts to jekyll" :tag "org2jekyll" :version "0.0.3" :group custom-declare-variable org2jekyll-blog-author "Blog entry author." :type string :require defalias org2jekyll/blog-author org2jekyll-source-directory "Path to the source directory." org2jekyll/source-directory org2jekyll-jekyll-directory "Path to Jekyll blog." org2jekyll/jekyll-directory org2jekyll-jekyll-drafts-dir "Relative path to drafts directory." org2jekyll/jekyll-drafts-dir org2jekyll-jekyll-posts-dir "Relative path to posts directory." org2jekyll/jekyll-posts-dir] 10)
#@33 File extension of Jekyll posts.
(defvar org2jekyll-jekyll-post-ext ".org" (#$ . 1586))
#@156 Default template for org2jekyll draft posts.
The `'%s`' will be replaced respectively by name, author, generated date, title,
 description and categories.
(defvar org2jekyll-jekyll-org-post-template nil (#$ . 1680))
(byte-code "\301\302\207" [org2jekyll-jekyll-org-post-template "#+STARTUP: showall\n#+STARTUP: hidestars\n#+OPTIONS: H:2 num:nil tags:nil toc:nil timestamps:t\n#+LAYOUT: %s\n#+AUTHOR: %s\n#+DATE: %s\n#+TITLE: %s\n#+DESCRIPTION: %s\n#+TAGS: %s\n#+CATEGORIES: %s\n\n" nil] 1)
#@75 Compute the folder name from a FOLDER-SOURCE and an optional FOLDER-NAME.
(defalias 'org2jekyll--optional-folder #[(folder-source &optional folder-name) "\302\303	\203 	\202\f \304#\207" [folder-source folder-name format "%s/%s" ""] 4 (#$ . 2177)])
#@48 Compute the input folder from the FOLDER-NAME.
(defalias 'org2jekyll-input-directory #[(&optional folder-name) "\302	\"\207" [org2jekyll-source-directory folder-name org2jekyll--optional-folder] 3 (#$ . 2434)])
#@58 Compute the output folder from the optional FOLDER-NAME.
(defalias 'org2jekyll-output-directory #[(&optional folder-name) "\302	\"\207" [org2jekyll-jekyll-directory folder-name org2jekyll--optional-folder] 3 (#$ . 2652)])
#@30 Turn a string S into a slug.
(defalias 'org2jekyll--make-slug #[(s) "\301\302\303\301\304\305#\227#\207" [s replace-regexp-in-string " " "-" "[][(){}!#$~^\\]" ""] 7 (#$ . 2881)])
#@29 Escape a string S for YAML.
(defalias 'org2jekyll--yaml-escape #[(s) "\301\302\"\204 \301\303\"\203 \303\304\303\305#\303Q\207\207" [s string-match ":" "\"" replace-regexp-in-string "\\\\\""] 5 (#$ . 3067)])
#@32 Generate a formatted now date.
(defalias 'org2jekyll-now #[nil "\300\301!\207" [format-time-string "%Y-%m-%d %a %H:%M"] 2 (#$ . 3287)])
#@246 Compute default headers.
BLOG-LAYOUT is the layout of the post.
BLOG-AUTHOR is the author.
POST-DATE is the date of the post.
POST-TITLE is the title.
POST-DESCRIPTION is the description.
POST-TAGS is the tags
POST-CATEGORIES is the categories.
(defalias 'org2jekyll-default-headers-template #[(blog-layout blog-author post-date post-title post-description post-tags post-categories) "\306	\n\307\f!	&\207" [org2jekyll-jekyll-org-post-template blog-layout blog-author post-date post-title post-description format org2jekyll--yaml-escape post-tags post-categories] 9 (#$ . 3430)])
#@60 Compute the draft's filename from the DRAFT-DIR and TITLE.
(defalias 'org2jekyll--draft-filename #[(draft-dir title) "\303	!\nQ\207" [draft-dir title org2jekyll-jekyll-post-ext org2jekyll--make-slug] 3 (#$ . 4023)])
#@17 Read the title.
(defalias 'org2jekyll--read-title #[nil "\300\301!\207" [read-string "Title: "] 2 (#$ . 4246)])
#@23 Read the description.
(defalias 'org2jekyll--read-description #[nil "\300\301!\207" [read-string "Description: "] 2 (#$ . 4364)])
#@16 Read the tags.
(defalias 'org2jekyll--read-tags #[nil "\300\301!\207" [read-string "Tags (csv): "] 2 (#$ . 4500)])
#@22 Read the categories.
(defalias 'org2jekyll--read-categories #[nil "\300\301!\207" [read-string "Categories (csv): "] 2 (#$ . 4621)])
#@56 Input PROMPT with possibilities limited to COLLECTION.
(defalias 'org2jekyll--input-read #[(prompt collection) "\302	\303\304$\207" [prompt collection ido-completing-read nil require-match] 5 (#$ . 4760)])
#@80 Initialize a DRAFT-FILE or current buffer routine.
:: () -> [(Symbol, String)]
(defalias 'org2jekyll--init-buffer-metadata #[nil "\301\302\303 \304\305\306\307\"\310\311 \312\313 \314\315 \316\317 \257\207" [org2jekyll-blog-author :author :date org2jekyll-now :layout org2jekyll--input-read "Layout: " ("post" "default") :title org2jekyll--read-title :description org2jekyll--read-description :tags org2jekyll--read-tags :categories org2jekyll--read-categories] 14 (#$ . 4973)])
#@78 Given an existing buffer, add the needed metadata to make it a post or page.
(defalias 'org2jekyll-init-current-buffer #[nil "\306 \307\310\"\307\311\"\307\312\"\307\313\"\307\314\"\307\315\"\307\316\"\212r\317 q\210eb\210\320	\n\f&c.\n\207" [metadata author date layout title description org2jekyll--init-buffer-metadata plist-get :author :date :layout :title :description :tags :categories buffer-name org2jekyll-default-headers-template tags categories] 8 (#$ . 5460) nil])
#@198 Create a new Jekyll blog post with TITLE.
The `'%s`' will be replaced respectively by the blog entry name, the author, the
 generated date, the title, the description, the tags and the categories.
(defalias 'org2jekyll-create-draft #[nil "\306 \307\310\"\307\311\"\307\312\"\307\313\"\307\314\"\307\315\"\307\316\"\317\320!\f\"\321!\204k \322\323\324!! !\325\216r q\210\326	\n\f&c\210\327c\210)r q\210\330\331\211!\331\332%\210,\333!.	\207" [metadata author date layout title description org2jekyll--init-buffer-metadata plist-get :author :date :layout :title :description :tags :categories org2jekyll--draft-filename org2jekyll-input-directory file-exists-p get-buffer-create generate-new-buffer-name " *temp file*" ((byte-code "\301!\203\n \302!\210\301\207" [#1=#:temp-buffer buffer-name kill-buffer] 2)) org2jekyll-default-headers-template "* " write-region nil 0 find-file tags categories org2jekyll-jekyll-drafts-dir draft-file #1# #2=#:temp-file] 8 (#$ . 5968) nil])
(defalias 'org2jekyll/create-draft! 'org2jekyll-create-draft)
#@26 List the content of DIR.
(defalias 'org2jekyll--list-dir #[(dir) "\301!\207" [dir find-file] 2 (#$ . 7051)])
#@25 Lists the posts folder.
(defalias 'org2jekyll-list-posts #[nil "\301\302!!\207" [org2jekyll-jekyll-posts-dir org2jekyll--list-dir org2jekyll-output-directory] 3 (#$ . 7167) nil])
(defalias 'org2jekyll/list-posts 'org2jekyll-list-posts)
#@25 List the drafts folder.
(defalias 'org2jekyll-list-drafts #[nil "\301\302!!\207" [org2jekyll-jekyll-drafts-dir org2jekyll--list-dir org2jekyll-input-directory] 3 (#$ . 7410) nil])
(defalias 'org2jekyll/list-drafts 'org2jekyll-list-drafts)
#@56 Gets the header value of the option OPT from a buffer.
(defalias 'org2jekyll-get-option-at-point #[(opt) "\302\226\227D!\212eb\210\303	\304\305\306$\205 \307\310!*\207" [opt regexp org-make-options-regexp re-search-forward nil t 1 match-string-no-properties 2] 5 (#$ . 7656)])
#@30 Return the ORGFILE's OPTION.
(defalias 'org2jekyll-get-option-from-file #[(orgfile option) "\303\304!rq\210\305\216\306	!\205 \307	!\210eb\210\310\n!+\207" [#1=#:temp-buffer orgfile option generate-new-buffer " *temp*" ((byte-code "\301!\203\n \302!\210\301\207" [#1# buffer-name kill-buffer] 2)) file-exists-p insert-file-contents org2jekyll-get-option-at-point] 2 (#$ . 7943)])
#@31 Return the ORGFILE's OPTIONS.
(defalias 'org2jekyll-get-options-from-file #[(orgfile options) "\303\304!rq\210\305\216\306	!\205 \307	!\210\310\311\n\"+\207" [#1=#:temp-buffer orgfile options generate-new-buffer " *temp*" ((byte-code "\301!\203\n \302!\210\301\207" [#1# buffer-name kill-buffer] 2)) file-exists-p insert-file-contents mapcar #[(option) "\212eb\210\301!)B\207" [option org2jekyll-get-option-at-point] 3]] 3 (#$ . 8334)])
#@86 Determine if the current ORG-FILE's layout.
Depends on the metadata header #+LAYOUT.
(defalias 'org2jekyll-layout #[(org-file) "\301\302\"\207" [org-file org2jekyll-get-option-from-file "layout"] 3 (#$ . 8784)])
(defalias 'org2jekyll-article-p 'org2jekyll-layout)
#@51 Keys to map from org headers to jekyll's headers.
(defvar org2jekyll-map-keys '(("title" . "title") ("categories" . "categories") ("tags" . "tags") ("date" . "date") ("description" . "excerpt") ("author" . "author") ("layout" . "layout")) (#$ . 9055))
#@69 Given an ORG-METADATA map, return a yaml one with transformed data.
(defalias 'org2jekyll--org-to-yaml-metadata #[(org-metadata) "\301\302\"\207" [org-metadata mapcar #[(it) "\302@	\"AB\207" [it org2jekyll-map-keys assoc-default] 3]] 3 (#$ . 9313)])
#@28 Convert org TIMESTAMP to .
(defalias 'org2jekyll--convert-timestamp-to-yyyy-dd-mm #[(timestamp) "\301\302\303\304\305!\"\"\207" [timestamp format-time-string "%Y-%m-%d" apply encode-time org-parse-time-string] 6 (#$ . 9572)])
#@44 Determine if we are in old version or not.
(defalias 'org2jekyll--old-org-version-p #[nil "\300\301!\207" [boundp org-element-block-name-alist] 2 (#$ . 9805)])
#@63 Given a list of ORG-METADATA, compute the yaml header string.
(defalias 'org2jekyll--to-yaml-header #[(org-metadata) "\305 \203	 \306\202\n \307\211\211\211A\242	@\310\311\312\211\n\313\314\315\316\f!\"BB\313\"\"\",\207" [#1=#:input0 #2=#:--dash-source-2-- begin end org-metadata org2jekyll--old-org-version-p ("#+BEGIN_HTML" "#+END_HTML\n") ("#+BEGIN_EXPORT HTML" "#+END_EXPORT\n") s-join "\n" -snoc "---" mapcar #[(it) "\301\302@A#\207" [it format "%s: %s"] 4] org2jekyll--org-to-yaml-metadata] 11 (#$ . 9971)])
#@50 Transform a STR-CSV entries into a yaml entries.
(defalias 'org2jekyll--csv-to-yaml #[(str-csv) "\301\302\303\304\305\303\306\304\211P##!P\207" [str-csv "\n" s-trim s-replace "," "\n- " ", "] 10 (#$ . 10499)])
#@144 Given a DATE and an ORG-FILE, compute a ready jekyll file name.
If the current path contains the `'org2jekyll-jekyll-drafts-dir`', removes it.
(defalias 'org2jekyll--compute-ready-jekyll-file-name #[(date org-file) "\305\306\307	!#\310	!\311\312\313\311\305\314\f\"\315\305\316\n###*\207" [date org-file temp-org-jekyll-directory temp-org-jekyll-filename org2jekyll-jekyll-drafts-dir format "%s-%s" file-name-nondirectory file-name-directory replace-regexp-in-string "//" "/" "%s" "" "%s%s"] 10 (#$ . 10717)])
#@115 Given DATE, ORG-FILE and YAML-HEADERS, copy content as org-jekyll ready file.
This returns the new filename path.
(defalias 'org2jekyll--copy-org-file-to-jekyll-org-file #[(date org-file yaml-headers) "\306	\"\211\307\310\311!!\312\216rq\210\313	!\210eb\210\314!c\210)rq\210\315\316\211\f\316\317%\210,\n)\207" [date org-file jekyll-filename #1=#:temp-buffer #2=#:temp-file yaml-headers org2jekyll--compute-ready-jekyll-file-name get-buffer-create generate-new-buffer-name " *temp file*" ((byte-code "\301!\203\n \302!\210\301\207" [#1# buffer-name kill-buffer] 2)) insert-file-contents org2jekyll--to-yaml-header write-region nil 0] 7 (#$ . 11238)])
#@113 Given KEY, ORG-DATA and DEFAULT-VALUE, return the value associated with key.
Return DEFAULT-VALUE if not found.
(defalias 'org2jekyll-assoc-default #[(key org-data default-value) "\304	\305\n$\211\203 \202 \n)\207" [key org-data default-value data assoc-default nil] 6 (#$ . 11905)])
#@59 The needed headers for org buffer for org2jekyll to work.
(defvar org2jekyll-header-metadata nil (#$ . 12200))
(byte-code "\301\302\207" [org2jekyll-header-metadata (("title" quote mandatory) ("date") ("categories" quote mandatory) ("tags") ("description" quote mandatory) ("author") ("layout" quote mandatory)) nil] 1)
#@138 Check that the mandatory header metadata in ORG-METADATA are provided.
Return the error messages if any or nil if everything is alright.
(defalias 'org2jekyll-check-metadata #[(org-metadata) "\303\304\305\"!\306\307\310\311\312	\"\"!\211\205 \n\313\230?\205 \n*\207" [org2jekyll-header-metadata mandatory-values error-messages -compose #[(l) "\301\302\"\207" [l mapcar car] 3] #[(l) "\301\302\"\207" [l -filter cdr] 3] s-trim s-join "\n" mapcar #[(it) "\302	\"?\205 \303\304\211\226#\207" [it org-metadata assoc-default format "- The %s is mandatory, please add '#+%s' at the top of your org buffer."] 4] ""] 7 (#$ . 12528)])
#@191 Given an ORG-FILE, return its org metadata.
If non-mandatory values are missing, they are replaced with dummy ones.
Otherwise, display the error messages about the missing mandatory values.
(defalias 'org2jekyll-read-metadata #[(org-file) "\305\306\"\307\n	\"\310!\211\203 \311\312\f\"\202S \313\314\313\315#B\316\314\316\317#B\320\321\314\320\322 #!B\323\324\314\323\325#!B\326\324\314\326\327#!B\330\314\330\331#B\332\314\332\331#B\257+\207" [org2jekyll-header-metadata org-metadata-list org-file org-metadata error-messages mapcar car org2jekyll-get-options-from-file org2jekyll-check-metadata format "This org-mode file is missing mandatory header(s):\n%s\nPublication skipped" "layout" org2jekyll-assoc-default "post" "title" "dummy-title-should-be-replaced" "date" org2jekyll--convert-timestamp-to-yyyy-dd-mm org2jekyll-now "categories" org2jekyll--csv-to-yaml "dummy-category-should-be-replaced" "tags" "dummy-tags-should-be-replaced" "author" "" "description"] 12 (#$ . 13171)])
#@71 Execute ACTION-FN function after checking metadata from the ORG-FILE.
(defalias 'org2jekyll-read-metadata-and-execute #[(action-fn org-file) "\305!\306!\2037 \307!\211;\203 \310\n!\2023 \311\312\313\n\"!\203& \314\202' \315\f\n\"\210\316\317	#))\202; \316\320	\")\207" [org-file filename-non-dir org-metadata page-or-post action-fn file-name-nondirectory org2jekyll-article-p org2jekyll-read-metadata org2jekyll-message org2jekyll-post-p assoc-default "layout" "Post" "Page" format "%s '%s' published!" "'%s' is not an article, publication skipped!"] 5 (#$ . 14178)])
#@21 Log formatted ARGS.
(defalias 'org2jekyll-message #[(&rest args) "\301\302\303\304@\"A#\207" [args apply message format "org2jekyll - %s"] 5 (#$ . 14761)])
#@49 Publish as post with ORG-METADATA the ORG-FILE.
(defalias 'org2jekyll--publish-post-org-file-with-metadata #[(org-metadata org-file) "\305\306\"\307\305\310\"	#\311\n\312\f\"\"\210\313\n!*\207" [org-metadata org-file jekyll-filename blog-project org-publish-project-alist assoc-default "layout" org2jekyll--copy-org-file-to-jekyll-org-file "date" org-publish-file assoc delete-file] 5 (#$ . 14925)])
#@29 Publish ORG-FILE as a post.
(defalias 'org2jekyll-publish-post #[(org-file) "\301\302\"\207" [org-file org2jekyll-read-metadata-and-execute org2jekyll--publish-post-org-file-with-metadata] 3 (#$ . 15337)])
#@49 Publish as page with ORG-METADATA the ORG-FILE.
(defalias 'org2ekyll--publish-page-org-file-with-metadata #[(org-metadata org-file) "\306\307\"\310\n!\311\312\313\n\"\"\314\n\f\315\211\211%\210\f\316\317\320!!\321\216rq\210\322\f!\210eb\210\323!c\210\324\f!\210\325\f\326	\"\"\210)rq\210\327\330\211\330\331%\210,\332\f!+\207" [org-metadata blog-project org-file ext temp-file #1=#:temp-buffer assoc-default "layout" file-name-extension format "%sorg2jekyll" s-chop-suffix copy-file t get-buffer-create generate-new-buffer-name " *temp file*" ((byte-code "\301!\203\n \302!\210\301\207" [#1# buffer-name kill-buffer] 2)) insert-file-contents org2jekyll--to-yaml-header write-file org-publish-file assoc write-region nil 0 delete-file #2=#:temp-file org-publish-project-alist] 6 (#$ . 15550)])
#@29 Publish ORG-FILE as a page.
(defalias 'org2jekyll-publish-page #[(org-file) "\301\302\"\207" [org-file org2jekyll-read-metadata-and-execute org2ekyll--publish-page-org-file-with-metadata] 3 (#$ . 16366)])
#@48 Determine if the LAYOUT corresponds to a post.
(defalias 'org2jekyll-post-p #[(layout) "\301\230\207" [layout "post"] 2 (#$ . 16578)])
#@48 Determine if the LAYOUT corresponds to a page.
(defalias 'org2jekyll-page-p #[(layout) "\301\230\207" [layout "default"] 2 (#$ . 16720)])
#@28 Publish the 'web' project.
(defalias 'org2jekyll-publish-web-project #[nil "\300\301!\210\302\303!\207" [org2jekyll-message "Publish `'web`' project (images, css, js, etc...)." org-publish-project "web"] 2 (#$ . 16865)])
#@142 Publish the current org file as post or page depending on the chosen layout.
Layout `'post`' is a jekyll post.
Layout `'default`' is a page.
(defalias 'org2jekyll-publish #[nil "\303\304!\211\211\305p!L\210)\306\307\310!\311\n\312\313\314\315\316D\315D\317FE\"\311\n\320\"\311\n\321\"\211*\207" [#1=#:--cl-org-file-- #2=#:v it make-symbol "--org-file--" buffer-file-name nil deferred:next #[nil "\300\301\302!!\203\n \303\207\304\207" [org2jekyll-post-p org2jekyll-get-option-at-point "layout" org2jekyll-publish-post org2jekyll-publish-page] 3] deferred:nextc lambda (&rest --cl-rest--) apply quote #[(#3=#:G70166 publish-fn) "	J!\207" [publish-fn #3#] 2] --cl-rest-- #[(final-message) "\301 \210\207" [final-message org2jekyll-publish-web-project] 1] #[(final-message) "\301!\207" [final-message org2jekyll-message] 2]] 9 (#$ . 17093) nil])
(defalias 'org2jekyll/publish! 'org2jekyll-publish)
#@43 Default Bindings map for org2jekyll mode.
(defvar org2jekyll-mode-map nil (#$ . 18005))
(byte-code "\302 \303\304\305#\210\303\306\307#\210\303\310\311#\210\303\312\313#\210\303\314\315#\210)\302\207" [map org2jekyll-mode-map make-sparse-keymap define-key ".n" org2jekyll-create-draft ".p" org2jekyll-publish ".P" org2jekyll-publish-posts ".l" org2jekyll-list-posts ".d" org2jekyll-list-drafts] 4)
#@24 Publish all the posts.
(defalias 'org2jekyll-publish-posts #[nil "\301\302\303!\304\305\"\211)\207" [it nil deferred:next #[nil "\305\306\307\310	\"!\311\211\203) @\312\313\f!!\203 \fB)\nTA\211\204 *\237)\207" [#1=#:result org-publish-project-alist it-index #2=#:list it nil org-publish-get-base-files assoc "post" 0 org2jekyll-post-p org2jekyll-article-p] 5] deferred:nextc #[(posts) "\301\302\"\207" [posts mapc org2jekyll-publish-post] 3]] 4 (#$ . 18422) nil])
(defalias 'org2jekyll/publish-posts! 'org2jekyll-publish-posts)
#@24 Publish all the pages.
(defalias 'org2jekyll-publish-pages #[nil "\301\302\303!\304\305\"\211)\207" [it nil deferred:next #[nil "\305\306\307\310	\"!\311\211\203) @\312\313\f!!\203 \fB)\nTA\211\204 *\237)\207" [#1=#:result org-publish-project-alist it-index #2=#:list it nil org-publish-get-base-files assoc "default" 0 org2jekyll-page-p org2jekyll-article-p] 5] deferred:nextc #[(pages) "\301\302\"\207" [pages mapc org2jekyll-publish-page] 3]] 4 (#$ . 18974) nil])
(defalias 'org2jekyll/publish-pages! 'org2jekyll-publish-pages)
#@99 Non-nil if Org2jekyll mode is enabled.
Use the command `org2jekyll-mode' to change this variable.
(defvar org2jekyll-mode nil (#$ . 19529))
(make-variable-buffer-local 'org2jekyll-mode)
#@208 Functionality for publishing the current org-mode post to jekyll.
With no argument, the mode is toggled on/off.
Non-nil argument turns mode on.
Nil argument turns mode off.

Commands:
\{org2jekyll-mode-map}
(defalias 'org2jekyll-mode #[(&optional arg) "\303 	\304=\203 \n?\202 \305	!\306V\307\310\n\203 \311\202 \312\"\210\313\314!\203@ \303 \2033 \303 \232\203@ \315\316\n\203= \317\202> \320\"\210)\321 \210\n\207" [#1=#:last-message arg org2jekyll-mode current-message toggle prefix-numeric-value 0 run-hooks org2jekyll-mode-hook org2jekyll-mode-on-hook org2jekyll-mode-off-hook called-interactively-p any message "Org2jekyll mode %sabled" "en" "dis" force-mode-line-update] 3 (#$ . 19722) (list (or current-prefix-arg 'toggle))])
#@180 Hook run after entering or leaving `org2jekyll-mode'.
No problems result if this variable is not bound.
`add-hook' automatically binds it.  (This is true for all hook variables.)
(defvar org2jekyll-mode-hook nil (#$ . 20470))
(byte-code "\301\302\303\304\211%\210\305\306!\207" [org2jekyll-mode-map add-minor-mode org2jekyll-mode " o2j" nil provide org2jekyll] 6)
