;;; pacmacs-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (or (file-name-directory #$) (car load-path)))

;;;### (autoloads nil "pacmacs" "pacmacs.el" (22335 468 912324 742000))
;;; Generated autoloads from pacmacs.el

(autoload 'pacmacs-start "pacmacs" "\


\(fn)" t nil)

(autoload 'pacmacs-score "pacmacs" "\


\(fn)" t nil)

;;;***

;;;### (autoloads nil nil ("pacmacs-anim.el" "pacmacs-board.el" "pacmacs-pkg.el"
;;;;;;  "pacmacs-render.el" "pacmacs-score.el" "pacmacs-utils.el"
;;;;;;  "pacmacs-vector.el" "pacmacs-walls.el") (22335 470 571655
;;;;;;  92000))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; pacmacs-autoloads.el ends here
