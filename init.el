;;package management
(load "package")
(package-initialize)
(add-to-list 'package-archives
	     '("melpa" . "http://melpa.org/packages/") t)
(setq package-archive-enable-list '(("melpa" def magit)))

;;disable splash screen
(setq inhibit-splash-screen t
      inhibit-scratch-message nil)

;;remove scroll-bar, menu bar  and tool bar
(menu-bar-mode -1)
(scroll-bar-mode -1)
(tool-bar-mode -1)
(setq column-number-mode t)
(show-paren-mode t)
(setq echo-keystrokes 0.1
      use-dialog-box nil)
(display-battery-mode t)
(display-time-mode t)
;;display settings
(setq-default indicate-empty-lines t)
(when (not indicate-empty-lines )
  (toggle-indicate-empty-lines))


;;marking and selection
(transient-mark-mode t)
(setq x-select-enable-clipboard nil)
(setq x-select-enable-primary t)

;;indentation
(setq tab-width 2
      indent-tabs-mode nil)

;;disable backup file
(setq make-backup-files nil)

;;yes or no by y or n
(defalias 'yes-or-no-p 'y-or-n-p)


;;key bindings
(global-set-key (kbd "RET") 'newline-and-indent)
(global-set-key (kbd "C-;") 'comment-or-uncomment-region)
(global-set-key (kbd "M-/") 'hippie-expand)
(global-set-key (kbd "C-+") 'text-scale-increase)
(global-set-key (kbd "C--") 'text-scale-decrease)
(global-set-key (kbd "C-c C-k") 'compile)
(global-set-key (kbd "C-x g") 'magit-status)


;;org mode settings

(require 'org)
(require 'org-install)
(setq org-log-done t
      org-todo-keywords '((sequence "TODO" "INPROGRESS" "DONE"))
      org-todo-keyword-faces '(("INPROGRESS" . (:foreground "blue" :weight bold))))

;;ido mode
;;(ido-mode t)
;;(setq ido-use-virtual-buffers t)


;;org-reveal
(require 'ox-reveal)
(setq org-reveal-root "file:///mnt/codeds/github-repos/reveal.js/reveal.js")

;;magit full combo
(require 'magit)

;;yasnippet loading
(add-to-list 'load-path
	     "~/.emacs.d/plugins/yasnippet")
(require 'yasnippet)
(yas-global-mode 1)


;;autopair
(require 'autopair)

;;auto-complete
(require 'auto-complete-config)
(ac-config-default)

;;erb mode
(add-to-list 'auto-mode-alist '("\\.erb$" . web-mode))

;;indentation and buffer cleanup
(defun untabify-buffer ()
  (interactive)
  (untabify (point-min) (point-max)))

(defun indent-buffer ()
  (interactive)
  (indent-region (point-min) (point-max)))

;;nikola export and html5slide
(require 'ox-nikola)
(require 'ox-html5slide)

;;emms player
(require 'emms-setup)
(emms-all)
(emms-default-players)


;;prolog mode
;;(setq auto-mode-alist
;;      (cons ( cons "\\.pl" 'prolog-mode)
;;	    auto-mode-alist))
;;(setq prolog-inferior 'swi)


;;clisp
(setq inferior-lisp-program "/usr/bin/sbcl")

;;emms kebindings
    (global-set-key (kbd "C-c e <up>") 'emms-start)
    (global-set-key (kbd "C-c e <down>") 'emms-stop)
    (global-set-key (kbd "C-c e <left>") 'emms-previous)
    (global-set-key (kbd "C-c e <right>") 'emms-next)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["#242424" "#e5786d" "#95e454" "#cae682" "#8ac6f2" "#333366" "#ccaa8f" "#f6f3e8"])
 '(custom-enabled-themes (quote (leuven)))
 '(custom-safe-themes
   (quote
    ("0a072360db47f066a246d7254d126a347abb80707ab3f181fe97b4729bb318e9" "01ce486c3a7c8b37cf13f8c95ca4bb3c11413228b35676025fdf239e77019ea1" "3b0a350918ee819dca209cec62d867678d7dac74f6195f5e3799aa206358a983" "f0d8af755039aa25cd0792ace9002ba885fd14ac8e8807388ab00ec84c9497d7" "4f5bb895d88b6fe6a983e63429f154b8d939b4a8c581956493783b2515e22d6d" "40c66989886b3f05b0c4f80952f128c6c4600f85b1f0996caa1fa1479e20c082" default)))
 '(frame-brackground-mode (quote dark)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;;comint mode compilation ;; stack overflow
(define-key comint-mode-map (kbd "C-x c") 'compile)
(defun c-w-c ()
  (interactive)
  (setq current-prefix-arg '(4))
  (call-interactively 'compile))
;;(defun c-w-c ()
;;  (interactive)
;;  (call-interactively 'compile t (vector 21 (this-command-keys-vector))))
(global-set-key (kbd "C-x c") 'c-w-c)


;;org-beamer
(require 'ox-latex)
(add-to-list 'org-latex-classes
             '("beamer"
               "\\documentclass\[presentation\]\{beamer\}"
               ("\\section\{%s\}" . "\\section*\{%s\}")
               ("\\subsection\{%s\}" . "\\subsection*\{%s\}")
               ("\\subsubsection\{%s\}" . "\\subsubsection*\{%s\}")))

					;org-babel
;; org-babel configs
(org-babel-do-load-languages
 'org-babel-load-languages
 '((python . t)
   (C . t)
   (calc . t)
   (latex . t)
   (java . t)
   (ruby . t)
   (scheme . t)
   (sh . t)
   (sqlite . t)
   (js . t)))

;; helm mode configs
(require 'helm-config)
(require 'helm)
(helm-autoresize-mode t)
(global-set-key (kbd "M-x") 'helm-M-x)
(global-set-key (kbd "M-y") 'helm-show-kill-ring)
(global-set-key (kbd "C-x b") 'helm-mini)
(global-set-key (kbd "C-x C-f") 'helm-find-files)
(global-set-key (kbd "C-x k") 'ido-kill-buffer)
(global-set-key (kbd "C-c h g") 'helm-google-suggest)
(global-set-key (kbd "C-c h c") 'helm-calcul-expression)
(global-set-key (kbd "C-c h m") 'helm-man-woman)
(global-set-key (kbd "C-c h l") 'helm-locate)
(global-set-key (kbd "C-c h t") 'helm-top)
(global-set-key (kbd "C-c C-l") 'helm-minibuffer-history)
                 
 

;;defining font mapping
;;(set-face-attribute 'default nil :font "Droid Sans Mono-12" )
;;(set-frame-font FONT nil t)
